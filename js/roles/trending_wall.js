    //creates and returns a string with the header html the trending wall uses
    
    SiteVibes.createTWHeaderHTML = function(){
        return '<div class="title-seperator hp-featured-items">\
                    <h2 class="home-title" style="padding:10px;">Trending Now</h2>\
                </div>\
                <p style="padding:5px;" class="sv-page-description">We want to help you stay ahead of trends by sharing our most popular products in real time. From products that are being talked about on Facebook and Twitter, to products that people are purchasing.</p>'+
                (this.showScrollToTop ? '<div class="dept-anchor-link" id="sv_scrollToTop" style="display: block;">'+this.scrollToTopString+'</div>' : '')
    };

    //creates and appends html content built out of the json response of the  
    //trending wall api call

    /*
      !!!IMPORTANT IMPORTANT IMPORTANT!!!
      always include the onerror handler for product images!
      onerror="SiteVibes.imgErrorHandler(this);"
    */

    SiteVibes.createHtml = function(items){
        prodCount = 0;
        var item_html = '';
        
        var dC = document.querySelector('ul.atg_store_product');

        //social string
        var shareHTML = '';
        if(this.injectSocialButtons){
            var shareHTML = '<div class="sv_share_wrap">';
            
            for(i=0;i<this.socialShareButtons.length;i++){
                /*
                only add it if it has a share handler on the sitevibes object.
                strict naming convention is:
                '__SHARE_NAME__'+'Share', 
                so 'facebook' would be 'facebookShare
                */
                if(typeof(this[this.socialShareButtons[i]+'Share']) == 'function'){
                    shareHTML += '<div onclick="SiteVibes.'+this.socialShareButtons[i]+'Share(this);return false;" \
                          class="sv_share sv_'+this.socialShareButtons[i]+'"></div>';
                }
            }
            shareHTML += '</div>';
        }
        


        for(i=0;i<items.length;i++){
            var display_prices = '<span class="was">' + items[i].price + '</span>';
            var current_viewers = '';
            
            if ((items[i].trendingTime == 'Hot Now' || items[i].trendingTime == 'Just Added to Cart') && items[i].unique_count > 1) {
                
                var viewTag = ' people';
                if (items[i].unique_count == 1) viewTag = ' person';
                current_viewers = '<p  class="current-viewers">' + items[i].unique_count + viewTag + " viewing now" + '</p>';
            }

            //figure out trending type
            var tTypeObj = SiteVibes.determineTrendingType(items[i]);

            var trendingTypeHTML = '';
            trendingTypeHTML = 
            '<div class="sv-trending-type-wrap">\
                <i style="color:'+tTypeObj.color+'!important;" class="' + tTypeObj.iconClass + '"></i>'+
            '<p class="sv-trending-type">' + tTypeObj.tType + '</p></div>';

            var itemNum = items[i].description;
            var color = '';
                    
            var descs = items[i].description.split(',');
            
            var prices = decodeURIComponent(items[i].price).split(',');
            
            var was = prices
            var sale = 0;
            var save = 0;
            
            if(prices[1]){
                sale = prices[0];
                was  = prices[1];
                
                var fWas = was.replace(/Was\s\$/, '');
                var fSale = sale.replace(/\$/, '');

                save = (parseFloat(fWas) - parseFloat(fSale)).toFixed(2);
            }

            item_html += '<li class="sv-item-item item-item" >'+
                trendingTypeHTML+'\
                <a href="'+items[i].url+'" class="sv-image-link">\
                    '+shareHTML+
                    '<img onerror="SiteVibes.imgErrorHandler(this);" class="sv-item-image item-thumb" alt="'+items[i].name+'" src="'+items[i].image+'">'+
//                    '+(sale ? '<span class="item-flag item-flag-sale" style="left:72px;top:187px;">Sale</span>' : '')+'
                    '<span class="item-title">'+items[i].name+'</span>\
                    '+current_viewers+'\
                </a>\
                <span class="item-price '+(sale ? 'sale' : '')+'">'+(sale ? sale : was)+'</span>\
                '+(sale ?
                '<span class="item-price-meta">\
                    <span class="item-price-meta-was">'+was+'</span>'+
//                    <span class="item-price-meta-save">Save $'+save+'</span>
                '</span>' : '')+
                '<a class="view-details" href="'+items[i].url+'">View Details</a>'+
//                '<a onclick="event.preventDefault();SiteVibes.qvFix(this);" href="http://www.shoecarnival.com/global/modals/quickview.jsp?productId='+itemNum+'&amp;selectedColor='+color+'&amp;qv=y" class="item-qv button button-grey" style="left:72px;">Quick View</a>
            '</li>';
            
            
        
        };
        
        var cv = document.querySelectorAll('.current-viewers');
        for(i=0;i<cv.length;i++){
            this.fadeIn(cv[i]);
        }
        //toss it in the DOM
        dC.insertAdjacentHTML('beforeend', item_html);
    };


    //creates and appends the category tags used on the trending wall.  also 
    //registers the event handlers currently

    SiteVibes.createTags = function(response){
        var ref = this;

        var tagContainer = "<ul style='margin-top:15px;' class='sv-filter'>";
        //tagContainer.append('<p id="sv-filter-info"></p>');
        var fs = response[0].tags;
        var tclass = 'tags';
        if (this.tagName == '') tclass += ' sv-filter-name-selected';
        tagContainer += "<li><a href='#' class='" + tclass + "' >All</a></li>";
        tclass = 'tags';
        for (var key in fs) {

            if (fs.hasOwnProperty(key)) {
                if (fs[key]['term'] == decodeURIComponent(this.tagName)) tclass += ' sv-filter-name-selected';
                if (fs[key]['term'] !== this.preselect) {
                    tagContainer += '<li><a href="#" class="' + tclass + '" alt="' + fs[key]['term'] + '" >' + fs[key]['term'] + '</a></li>';
                }
                tclass = 'tags';
            }
        }
        tagContainer += "</ul>";
        
        var dC = document.querySelector(this.dataContainer);
        if(dC){
            dC.insertAdjacentHTML('beforeend', tagContainer);
        }

        //to make sure there are only one line of tags, we can check the height of the
        //first tag and see if the height of the parentNode == to it
        var tags = document.getElementsByClassName('tags');
        var baseHeight = tags[0].offsetHeight;
        var currHeight = tags[0].parentNode.parentNode.offsetHeight;
        var tagsLen = tags.length;
        var borders = 0;
        var styles = getComputedStyle(tags[0].parentNode.parentNode);
        borders += parseInt(+styles['borderBottomWidth'].replace(/[^\d\.\-]/g,''));
        borders += parseInt(+styles['borderTopWidth'].replace(/[^\d\.\-]/g,''));

        while(baseHeight != (currHeight-borders) && tags.length > 1){  //always leave min 1
            
            var el = tags[--tagsLen];
            el.parentNode.removeChild(el);
            currHeight = tags[0].parentNode.parentNode.offsetHeight;
        }

        //remember that we created tags
        this.tagsCreated = true;

        //lastly, set event listeners related to tags

        for(i=0;i<tags.length;i++){
            tags[i].addEventListener('click', function(e) {
                e.preventDefault();

                if(this.className == 'tags sv-filter-name-selected'){
                    //dont bother making an api call if they clicked the active tab
                    return;
                }

                ref.tagName = '';
                for(j=0;j<this.attributes.length;j++){
                    if(this.attributes[j].name == 'alt'){
                        ref.tagName = encodeURIComponent(this.attributes[j].value);
                        
                        break;
                    }
                }

                for(ii=0;ii<tags.length;ii++){
                    tags[ii].className = 'tags';
                }
                this.className = 'tags sv-filter-name-selected';
                ref.svCounter = 1;
                ref.trendingWallScrolls=0;
                sessionStorage.scrolls = 0;

                ref.apiCall();
            });
        }
    
    };

    //can be used to facilitate using quickview stuff easier without having to 
    //type out complicated inline code on elements we inject
    SiteVibes.qvFix = function(node){
        $.modal('<iframe src='+ node.href + ' height="550" width="850" scrolling="auto" id="qv">',
        { 
            overlayClose:true, 
            onShow: function(){
                $('#simplemodal-container').css('height','auto');
            }
        });
        return false;
    };


    SiteVibes.prepareTWPing = function(node){
        var session = this.uuid();
        var today = new Date();
        var fdate = today.getUTCFullYear()+"-"+
                   (today.getUTCMonth()+1)+"-"+
                    today.getUTCDate()+" "+today.getUTCHours()+":"+
                    today.getUTCMinutes()+":"+today.getUTCSeconds();
        this.q.session = session;
        this.q.time = fdate;

        var media = node.parentNode.parentNode.querySelector('.sv-item-image');
        media = (media && media.src ? media.src : '');
        this.q.image = media;

        var text  = node.parentNode.parentNode.parentNode.querySelector('.item-title');
        text = (text && text.textContent ? text.textContent : '');
        this.q.name = text;

        this.q.url             = node.parentNode.parentNode.href;
        this.q.host            = encodeURIComponent(location.host);

        this.q.productCategory = '';
        this.q.pagetype        = 'trending_wall';
        this.q.id              = encodeURIComponent(this.tmiID);
    
        this.q.price = encodeURIComponent(node.parentNode.parentNode.parentNode.querySelectorAll('.item-price')[0].textContent.trim());

        //if theres a sales price, this grabs the reg price
        var wasNode = node.parentNode.parentNode.parentNode.querySelector('.item-price-meta-was');
        if(wasNode && wasNode.textContent){
            this.q.price += ","+encodeURIComponent(wasNode.textContent);
        }
        this.ping();
    }
